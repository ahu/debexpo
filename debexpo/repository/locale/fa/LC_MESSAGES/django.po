# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-22 21:27+0000\n"
"PO-Revision-Date: 2022-11-23 14:54+0000\n"
"Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>\n"
"Language-Team: Persian <https://hosted.weblate.org/projects/debexpo/"
"repository/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.15-dev\n"

#: repository/models.py
msgid "Name"
msgstr "نام"

#: repository/models.py
msgid "Version"
msgstr "نگارش"

#: repository/models.py
msgid "Component"
msgstr "مولفه"

#: repository/models.py
msgid "Distribution"
msgstr "توزیع"

#: repository/models.py
msgid "Path"
msgstr "مسیر"

#: repository/models.py
msgid "Size"
msgstr "اندازه"

#: repository/models.py
msgid "SHA256"
msgstr "SHA256"
